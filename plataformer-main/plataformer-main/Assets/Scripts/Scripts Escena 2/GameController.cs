using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Referencias a los jugadores
    public GameObject player1;
    public GameObject player2;

    // �ndice del jugador actual (0 para player1, 1 para player2)
    private int currentPlayerIndex = 0;

    private Controller_Player player1Controller;
    private Controller_Player player2Controller;

    void Start()
    {
        // Obtener los componentes de los jugadores
        player1Controller = player1.GetComponent<Controller_Player>();
        player2Controller = player2.GetComponent<Controller_Player>();

        // Aseg�rate de que solo el jugador actual est� activo
        SetActivePlayer(currentPlayerIndex);
    }

    void Update()
    {
        // Cambiar jugador cuando se presiona la tecla C
        if (Input.GetKeyDown(KeyCode.C))
        {
            ChangePlayer();
        }
    }

    // Cambia el jugador activo
    private void ChangePlayer()
    {
        currentPlayerIndex = 1 - currentPlayerIndex; // Cambiar entre 0 y 1
        SetActivePlayer(currentPlayerIndex);
    }

    // Activa el jugador actual y desactiva el otro
    private void SetActivePlayer(int index)
    {
        if (index == 0)
        {
            player1.SetActive(true);
            player2.SetActive(false);
            player1Controller.enabled = true;
            player2Controller.enabled = false;
        }
        else
        {
            player1.SetActive(false);
            player2.SetActive(true);
            player1Controller.enabled = false;
            player2Controller.enabled = true;
        }
    }

    // M�todo para manejar la muerte del jugador
    public void PlayerDeath(GameObject player)
    {
        Debug.Log("Player " + player.name + " has died.");
        player.SetActive(false);
        // Opcional: l�gica adicional para manejar la muerte del jugador, como mostrar una pantalla de Game Over
    }
}
