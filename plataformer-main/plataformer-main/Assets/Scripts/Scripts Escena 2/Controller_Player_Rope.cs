using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRope : MonoBehaviour
{
    // Prefab de la cuerda
    public GameObject ropePrefab;
    // Punto desde donde se lanza la cuerda
    public Transform ropeSpawnPoint;
    // Longitud de la cuerda
    public float ropeLength = 10f;

    // Referencia a la cuerda actual
    private GameObject currentRope;

    void Update()
    {
        HandleRope();
    }

    private void HandleRope()
    {
        // Lanzar o retractar la cuerda con la tecla F
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (currentRope == null)
            {
                LaunchRope();
            }
            else
            {
                RetractRope();
            }
        }
    }

    private void LaunchRope()
    {
        // Instancia la cuerda y ajusta su longitud
        currentRope = Instantiate(ropePrefab, ropeSpawnPoint.position, Quaternion.identity);
        currentRope.transform.localScale = new Vector3(ropeLength, 1, 1); // Ajusta la escala para hacerla horizontal
        currentRope.transform.parent = ropeSpawnPoint; // Opcional: hacer que la cuerda sea hija del punto de lanzamiento
    }

    private void RetractRope()
    {
        // Destruye la cuerda actual
        Destroy(currentRope);
        currentRope = null;
    }
}
