using UnityEngine;

public class PlayerDoubleJump : MonoBehaviour
{
    public float jumpForce = 10f;
    public float doubleJumpForce = 7f;
    public int maxJumps = 2; // N�mero m�ximo de saltos, incluido el doble salto
    private int jumpsRemaining; // Saltos restantes

    private Rigidbody rb;
    private bool isGrounded; // Flag para saber si el jugador est� en el suelo
    public Transform groundCheck; // Punto desde donde se verifica si el jugador est� en el suelo
    public LayerMask groundMask; // Capa de objetos que representan el suelo

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jumpsRemaining = maxJumps;
    }

    void Update()
    {
        // Verificar si el jugador est� en el suelo
        isGrounded = Physics.CheckSphere(groundCheck.position, 0.2f, groundMask);

        // Resetear los saltos restantes si el jugador est� en el suelo
        if (isGrounded)
        {
            jumpsRemaining = maxJumps;
        }

        // Permitir el doble salto si quedan saltos restantes y se presiona la tecla de salto
        if (Input.GetKeyDown(KeyCode.Space) && jumpsRemaining > 0)
        {
            Jump();
        }
    }

    void Jump()
    {
        // Aplicar fuerza para el salto normal o el doble salto dependiendo de los saltos restantes
        if (jumpsRemaining == maxJumps)
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpForce, rb.velocity.z);
        }
        else
        {
            rb.velocity = new Vector3(rb.velocity.x, doubleJumpForce, rb.velocity.z);
        }
        jumpsRemaining--; // Disminuir los saltos restantes
    }
}
