using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player2 : MonoBehaviour
{
    public float jumpForce = 10f;
    public Rigidbody rb;
    private bool canJump;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Solo permite al jugador activo saltar
        if (Input.GetKeyDown(KeyCode.W) && canJump)
        {
            Jump();
        }
    }

    private void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        canJump = false; // Prevenir saltos m�ltiples
    }

    void OnCollisionEnter(Collision collision)
    {
        // Permitir saltar solo cuando el jugador toca el suelo
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
        }
    }
}
