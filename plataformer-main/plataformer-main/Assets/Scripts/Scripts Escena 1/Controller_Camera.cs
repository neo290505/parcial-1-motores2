﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    // Lista de objetos jugadores que la cámara seguirá.
    public List<GameObject> players;

    // Referencia a la cámara adjunta a este script.
    private Camera _camera;

    // Tiempo de amortiguación para suavizar el movimiento de la cámara.
    public float dampTime = 0.15f;

    // Tiempo de suavizado para algún otro aspecto del movimiento de la cámara.
    public float smoothTime = 2f;

    // Valor de zoom para ajustar el campo de visión de la cámara.
    public float zoomvalue = 5f;

    // Velocidad actual de la cámara, usada por SmoothDamp.
    private Vector3 velocity = Vector3.zero;

    // Velocidad actual del FOV, usada por SmoothDamp.
    private float zoomVelocity = 0.0f;

    // Flag para verificar si la cámara está enfocada en el jugador o en otro objeto.
    private bool isCameraFocusedOnPlayer = true;

    // Posición de enfoque de la cámara cuando no está en el jugador.
    public Transform focusPosition; // Asigna el objeto vacío en el inspector de Unity.

    void Start()
    {
        // Obtener la referencia al componente Camera adjunto a este objeto.
        _camera = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        // Verificar si la lista de jugadores no es nula y el índice actual de jugador es válido.
        if (players != null && players.Count > GameManager.actualPlayer && players[GameManager.actualPlayer] != null)
        {
            // Verificar si la cámara está enfocada en el jugador o en otro objeto.
            if (isCameraFocusedOnPlayer)
            {
                // Obtener la posición del jugador en coordenadas de vista de la cámara.
                Vector3 point = _camera.WorldToViewportPoint(players[GameManager.actualPlayer].transform.position);

                // Calcular el delta entre la posición del jugador y el punto central de la cámara.
                Vector3 delta = players[GameManager.actualPlayer].transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));

                // Calcular la nueva posición de destino para la cámara.
                Vector3 destination = transform.position + delta;

                // Suavizar el movimiento de la cámara hacia la nueva posición.
                transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

                // Calcular la distancia al jugador.
                float distanceToPlayer = delta.magnitude;

                // Ajustar el campo de visión de la cámara según la distancia al jugador.
                float targetFOV = Mathf.Lerp(60, 40, distanceToPlayer / zoomvalue); // Ajusta 60 y 40 según tus necesidades.
                _camera.fieldOfView = Mathf.SmoothDamp(_camera.fieldOfView, targetFOV, ref zoomVelocity, smoothTime);
            }
            else
            {
                // Mover la cámara hacia la posición de enfoque cuando no está enfocada en el jugador.
                Vector3 targetPosition = focusPosition.position;
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, dampTime);

                // Mantener el mismo campo de visión cuando no está enfocada en el jugador.
                _camera.fieldOfView = Mathf.SmoothDamp(_camera.fieldOfView, _camera.fieldOfView, ref zoomVelocity, smoothTime);
            }
        }

        // Cambiar el enfoque de la cámara al presionar la tecla "C".
        if (Input.GetKeyDown(KeyCode.C))
        {
            isCameraFocusedOnPlayer = !isCameraFocusedOnPlayer;
        }
    }
}
