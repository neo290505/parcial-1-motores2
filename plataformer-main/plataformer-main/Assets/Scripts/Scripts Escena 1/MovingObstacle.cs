using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    // Velocidad de movimiento del obst�culo
    public float moveSpeed = 5f;

    // Altura m�xima a la que se mover� el obst�culo
    public float maxHeight = 4f;

    // Altura m�nima a la que se mover� el obst�culo
    public float minHeight = -4f;

    // Referencia al GameManager
    public GameManager gameManager;

    // Update is called once per frame
    void Update()
    {
        // Mover el obst�culo hacia arriba o abajo
        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);

        // Si el obst�culo alcanza la altura m�xima o m�nima, invertir la direcci�n
        if (transform.position.y >= maxHeight || transform.position.y <= minHeight)
        {
            moveSpeed *= -1;
        }
    }

    // M�todo llamado cuando el obst�culo colisiona con otro objeto
    private void OnTriggerEnter(Collider other)
    {
        // Si el objeto con el que colisiona es un jugador
        if (other.CompareTag("Player"))
        {
           
        }
    }
}
