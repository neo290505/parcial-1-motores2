﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    // Fuerza de salto del jugador.
    public float jumpForce = 10;

    // Velocidad de movimiento del jugador.
    public float speed = 5;

    // Número de jugador.
    public int playerNumber;

    // Componente Rigidbody del jugador.
    public Rigidbody rb;

    // Componente BoxCollider del jugador.
    private BoxCollider col;

    // Capa de suelo para detectar si el jugador está en el suelo.
    public LayerMask floor;

    // Estructuras para almacenar información de los rayos lanzados.
    internal RaycastHit leftHit, rightHit, downHit;

    // Distancia de los rayos.
    public float distanceRay, downDistanceRay;

    // Variables para controlar el movimiento del jugador.
    private bool canMoveLeft, canMoveRight, canJump;
    internal bool onFloor;

    private void Start()
    {
        // Obtener componentes y configurar restricciones de movimiento.
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }

    public virtual void FixedUpdate()
    {
        // Verificar si es el turno de este jugador para controlarlo.
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();
        }
    }

    private void Update()
    {
        // Verificar si es el turno de este jugador para controlarlo.
        if (GameManager.actualPlayer == playerNumber)
        {
            Jump();

            // Determinar si el jugador puede moverse a la izquierda o derecha.
            canMoveLeft = !SomethingLeft();
            canMoveRight = !SomethingRight();

            // Determinar si el jugador puede saltar.
            canJump = IsOnSomething();
        }
        else
        {
            // Restringir el movimiento del jugador si no es su turno.
            if (onFloor)
            {
                rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                if (IsOnSomething())
                {
                    if (downHit.collider.gameObject.CompareTag("Player"))
                    {
                        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    }
                }
            }
        }
    }

    // Método para verificar si el jugador está en contacto con algo debajo.
    public virtual bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay);
    }

    // Método para verificar si hay algo a la derecha del jugador.
    public virtual bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out rightHit, transform.localScale.x / 1.8f);
    }

    // Método para verificar si hay algo a la izquierda del jugador.
    public virtual bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2.2f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out leftHit, transform.localScale.x / 1.8f);
    }

    private void Movement()
    {
        // Movimiento horizontal del jugador.
        if (Input.GetKey(KeyCode.A) && canMoveLeft)
        {
            rb.velocity = new Vector3(-speed, rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.D) && canMoveRight)
        {
            rb.velocity = new Vector3(speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }

    public virtual void Jump()
    {
        // Salto del jugador si puede hacerlo.
        if (Input.GetKeyDown(KeyCode.W) && canJump)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        // Verificar colisión con agua o suelo.
        if (collision.gameObject.CompareTag("Water"))
        {
            // Si el jugador colisiona con agua, el juego termina.
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            // Si el jugador colisiona con el suelo, se actualiza su estado.
            onFloor = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        // Cuando el jugador deja de colisionar con el suelo, se actualiza su estado.
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = false;
        }
    }
}
