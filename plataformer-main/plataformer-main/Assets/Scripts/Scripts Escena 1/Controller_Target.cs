﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Target : MonoBehaviour
{
    // Número de objetivo.
    public int targetNumber;

    // Indica si hay un jugador en el objetivo.
    public bool playerOnTarget;

    private void Start()
    {
        // Al inicio, no hay ningún jugador en el objetivo.
        playerOnTarget = false;
    }

    // Método llamado cuando un objeto entra en el área de activación del objetivo.
    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto que entra es un jugador.
        if (other.gameObject.CompareTag("Player"))
        {
            // Obtener el componente Controller_Player del jugador que entró en el objetivo.
            Controller_Player playerController = other.GetComponent<Controller_Player>();

            // Verificar si el jugador que entró en el objetivo tiene el mismo número que el objetivo actual.
            if (playerController.playerNumber == targetNumber)
            {
                // El jugador está en el objetivo.
                playerOnTarget = true;
                //Debug.Log("Player on Target");
            }
        }
    }

    // Método llamado cuando un objeto sale del área de activación del objetivo.
    private void OnTriggerExit(Collider other)
    {
        // Verificar si el objeto que sale es un jugador.
        if (other.gameObject.CompareTag("Player"))
        {
            // Obtener el componente Controller_Player del jugador que sale del objetivo.
            Controller_Player playerController = other.GetComponent<Controller_Player>();

            // Verificar si el jugador que sale del objetivo tiene el mismo número que el objetivo actual.
            if (playerController.playerNumber == targetNumber)
            {
                // El jugador ya no está en el objetivo.
                playerOnTarget = false;
                //Debug.Log("Player off Target");
            }
        }
    }
}
