﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    // Referencia al componente de texto que muestra el mensaje de fin de juego.
    public Text gameOverText;

    void Start()
    {
        // Asegurarse de que el texto de fin de juego esté oculto al inicio.
        gameOverText.gameObject.SetActive(false);

        // Restablecer el tiempo de juego.
        Time.timeScale = 1;
    }

    void Update()
    {
        // Verificar si el juego ha terminado.
        if (GameManager.gameOver)
        {
            // Detener el tiempo en el juego.
            Time.timeScale = 0;

            // Mostrar el mensaje de fin de juego.
            gameOverText.text = "Game Over";
            gameOverText.gameObject.SetActive(true);
        }
        // Verificar si la condición de victoria se ha cumplido.
        else if (GameManager.winCondition)
        {
            // Detener el tiempo en el juego.
            Time.timeScale = 0;

            // Mostrar el mensaje de victoria.
            gameOverText.text = "You Win";
            gameOverText.gameObject.SetActive(true);
        }
    }
}
