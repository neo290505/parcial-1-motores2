﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Trampoline : Controller_Player
{
    // Fuerza aplicada al jugador al colisionar con el trampolín.
    public float trampolineForce;

    // Método llamado cuando el jugador colisiona con otro objeto.
    public override void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto con el que colisiona el jugador es otro jugador.
        if (collision.gameObject.CompareTag("Player"))
        {
            // Obtener el componente Rigidbody del objeto con el que colisiona el jugador.
            Rigidbody otherRigidbody = collision.gameObject.GetComponent<Rigidbody>();

            // Aplicar una fuerza hacia arriba al otro jugador para simular el efecto del trampolín.
            otherRigidbody.AddForce(new Vector3(0, jumpForce * trampolineForce, 0), ForceMode.Impulse);
        }

        // Llamar al método OnCollisionEnter de la clase base para mantener la funcionalidad del jugador.
        base.OnCollisionEnter(collision);
    }
}
