using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            LoadNextScene();
        }
    }

    void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex); // Cargar la siguiente escena
            SceneManager.UnloadSceneAsync(currentSceneIndex); // Descargar la escena actual de forma as�ncrona
        }
        else
        {
            Debug.LogWarning("No hay m�s escenas disponibles para cargar.");
        }
    }
}
