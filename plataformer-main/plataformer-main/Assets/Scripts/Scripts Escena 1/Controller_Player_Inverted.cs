﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Inverted : Controller_Player
{
    // Override del método FixedUpdate para aplicar fuerza hacia arriba antes de llamar al FixedUpdate de la clase base.
    public override void FixedUpdate()
    {
        // Aplicar una fuerza hacia arriba para simular la gravedad invertida.
        base.rb.AddForce(new Vector3(0, 30f, 0));

        // Llamar al FixedUpdate de la clase base para mantener la funcionalidad principal del movimiento.
        base.FixedUpdate();
    }

    // Override del método IsOnSomething para verificar si el jugador está en contacto con algo encima de él.
    public override bool IsOnSomething()
    {
        // Esta implementación invierte la dirección del rayo de detección para verificar si hay algo encima del jugador.
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.up, out downHit, Quaternion.identity, downDistanceRay);
    }

    // Override del método Jump para permitir que el jugador salte hacia abajo cuando esté en contacto con algo.
    public override void Jump()
    {
        // Verificar si el jugador está en contacto con algo encima de él.
        if (IsOnSomething())
        {
            // Si el jugador está en contacto con algo encima y presiona la tecla de salto, aplicar una fuerza hacia abajo para simular el salto invertido.
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }
}
