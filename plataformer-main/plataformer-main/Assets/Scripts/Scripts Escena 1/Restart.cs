﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {
        // Llama al método GetInput para verificar si se presiona la tecla R.
        GetInput();
    }

    private void GetInput()
    {
        // Verifica si se presiona la tecla R para reiniciar el juego.
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Restablece el tiempo a su valor normal para evitar problemas con el tiempo pausado.
            Time.timeScale = 1;
            // Recarga la escena actual (escena 0).
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
