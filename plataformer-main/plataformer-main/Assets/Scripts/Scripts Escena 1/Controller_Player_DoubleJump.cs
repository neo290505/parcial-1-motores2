﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_DoubleJump : Controller_Player
{
    // Contador de saltos.
    private int jumpCounter = 0;

    // Método para verificar si el jugador está en contacto con algo debajo.
    public override bool IsOnSomething()
    {
        // Este método es heredado de la clase base Controller_Player.
        return base.IsOnSomething();
    }

    // Método para realizar el salto del jugador.
    public override void Jump()
    {
        // Verificar si el jugador está en el suelo.
        if (IsOnSomething())
        {
            // Si el jugador está en el suelo y presiona la tecla de salto, realizar el primer salto.
            if (Input.GetKeyDown(KeyCode.W))
            {
                jumpCounter = 1;
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
        else
        {
            // Si el jugador no está en el suelo y presiona la tecla de salto, realizar el doble salto si aún tiene saltos disponibles.
            if (Input.GetKeyDown(KeyCode.W) && jumpCounter > 0)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                jumpCounter--;
            }
        }
    }
}

